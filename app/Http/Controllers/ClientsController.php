<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Client;
use App\Entreprise;

class ClientsController extends Controller
{
    public function __construct()
    {
        //use this to apply middleware for all ClientsController's methods
        //$this->middleware('auth');

        //use only or except for specify de methode need to middleware
        $this->middleware('auth')->except(['index']);
    }

    public function create()
    {
        $entreprises = Entreprise::all();
        $client = new Client();
        return view('clients.create', compact('entreprises','client'));
    }

    public function index()
    {
        $clients = Client::all();
        //$clients = Client::where('status','=',1)-> get();
        //$clients = Client::status();


        /* $comment = Entreprise::find(1)->comments()->where('id', 'entreprise_id')->first();
        dd($comment);*/

        // return view('clients.index', ['data' => $clients]);
        return view('clients.index', compact('clients'));
    }

    public function store()
    {
        $data = request()->validate([
            'name' => 'required|min:5',
            'email' => 'required|email',
            'status' => 'required|integer',
            'entreprise_id' => 'required|integer'
        ]);


        /*
        $name = request('name');
        $email = request('email');
        $status = request('status');
        $client= new Client();
        $client -> name = $name;
        $client -> email = $email;
        $client -> status = $status;
        $client -> save();*/

        Client::create($data);

        return back();
    }

    public function show(Client $client)
    {
        // $client = Client::find($client);
        // $client = Client::where('id',$client)->firstOrFail();
        return view('clients.show', compact('client'));
    }

    public function edit(Client $client)
    {
        $entreprises = Entreprise::all();
        return view('clients.edit', compact('client', 'entreprises'));
    }

    public function update(Client $client)
    {
        $data = request()->validate([
            'name' => 'required|min:5',
            'email' => 'required|email',
            'status' => 'required|integer',
            'entreprise_id' => 'required|integer'
        ]);
        $client->update($data);

        return redirect('clients/'. $client->id);
    }

    public function destroy(Client $client){
        $client->delete();

        return redirect('clients');
    }
}
