<?php

namespace App\Http\Controllers;

use App\Mail\ContactMail;

use  Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;





class ContactController extends Controller
{
    //

    public function create(){

        return view('contact.create');
    }

    public function store(){

        $data = request()->validate([
            'name' => 'required|min:5',
            'email' => 'required|email',
            'message' => 'required'
        ]);

        Mail::to('test@test.com')->send(new ContactMail($data));



       /* session()->flash('message','Votre mail a bien été envoyé.');
        return redirect('contact');*/

        return redirect('contact')->with('message','Votre mail a bien été envoyé.');
    }


}
