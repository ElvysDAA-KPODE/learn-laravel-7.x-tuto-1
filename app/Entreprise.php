<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Entreprise extends Model
{
    //
    protected $guarded =[];

    public function clients(){
        return $this->hasMany('App\Client');
    }
}
