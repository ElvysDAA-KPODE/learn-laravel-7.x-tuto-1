@component('mail::message')
# Bonjour

Vous avez reçu un message de la part de  {{ $data['name'] }} ({{$data['email'] }})

Message

{{ $data['message'] }}

@component('mail::button', ['url' => ''])
Button Text
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
