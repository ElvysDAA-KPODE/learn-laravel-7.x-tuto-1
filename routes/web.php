<?php

use Illuminate\Support\Facades\Route;
//use App\Http\Controllers\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
   return view('welcome');
});*/

/*Route::get('contact',function(){
    return view('contact');
});*/

Route::view('/','welcome');
//Route::view('contact','contacts');
Route::view('a-propos','a-propos')->middleware('test');

//Client
/*Route::get('clients/create','ClientsController@create');
Route::get('clients','ClientsController@index');
Route::post('clients','ClientsController@store');
Route::get('clients/{client}','ClientsController@show');
Route::get('clients/{client}/edit','ClientsController@edit');
Route::patch('clients/{client}','ClientsController@update');
Route::delete('clients/{client}','ClientsController@destroy');*/

Auth::routes();

//Route::resource('clients','ClientsController')->middleware('auth');
Route::resource('clients','ClientsController');

//Contact
Route::get('contactez-nous','ContactController@create')->name('contact.create');
Route::post('contactez-nous','ContactController@store')->name('contact.store');


Route::get('/home', 'HomeController@index')->name('home');



